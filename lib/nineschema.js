var Ajv = require('ajv');
var schema = {
  "required":["payload"],
  "type":"object",
  "properties": {
      "payload":{
		  "type":"array",
		  "items":{
				"properties": {
					"drm":{"type":"boolean"},
					"episodeCount":{"type":"integer"},
					"image":{
						"type":"object",
						"properties":{
							"showImage":{"type":"string"}
						}
					},
					"slug":{"type":"string"},
					"title":{"type":"string"}
				}
		    }
		}
    }
};

module.exports = {
    validateStructure: function(obj){
        var ajv = new Ajv();
        return ajv.validate(schema, obj);
    }
}