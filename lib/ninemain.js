var nineschema = require('./nineschema');
var ninefilter = require('./ninefilter');

module.exports = function listener(request, response){
	if(request.method == 'POST'){
		var body = '';
		request.on('data', function listener(data){
			body+=data;
		});
		request.on('end', function listener(){
			var objRequest = tryParseJSON(body);
			if(!objRequest){			//if JSON parse unsuccessful
				send400Response(response);
			}
			else if(nineschema.validateStructure(objRequest)){
				sendFilteredResponse(response, objRequest);
			}
			else{
				send400Response(response);
			}
		});
	}
}

var sendFilteredResponse = function(response, objJSON){
	response.writeHead(200, {"Content-Type": "application/json"});
	var objResponse = ninefilter.filterWithDrmAndEpisodes(objJSON);
	response.write(JSON.stringify(objResponse));
	response.end();
}

var send400Response = function(response){
	response.writeHead(400, {"Content-Type": "application/json"});
	var error = {
		error: "Could not decode request: JSON parsing failed"
	};
	response.write(JSON.stringify(error));
	response.end();
};

var tryParseJSON = function(string){
	try{
		var objJSON = JSON.parse(string);
		if(typeof objJSON === "object" && objJSON !== null && objJSON){
			return objJSON;
		}
		else{
		}
	}
	catch(e){
	}
	return false;
}