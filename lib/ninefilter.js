module.exports = {
    filterWithDrmAndEpisodes : function(objRequest){
		var objResponse = {response:[]};
        for(var i = 0; i < objRequest.payload.length; i++){
            var objShowFull = objRequest.payload[i];
            if(objShowFull.drm && objShowFull.episodeCount > 0 && 
            objShowFull.image != undefined && objShowFull.slug != undefined && objShowFull.title != undefined){
                if(objShowFull.image.showImage != undefined){
                    var objShowFiltered = makeFilteredShowObject(objShowFull);
                    objResponse.response.push(objShowFiltered);
                }
            }
        }
        return objResponse;
    }
}

var makeFilteredShowObject = function(objShow){
    var objShowFiltered = {
		image: objShow.image.showImage,
        slug: objShow.slug,
        title: objShow.title
    };
    
    return objShowFiltered;
}