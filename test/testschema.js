var assert = require('chai').assert;
var validateStructure = require("../lib/nineschema").validateStructure;
var objfortest = require("./objfortest");

describe('nineschema', function(){
    describe('validateStructure', function() {
        it('true to a standard request', function(){
            var result = validateStructure(objfortest.fullObj);
            assert.equal(result, true, "accepts the standard request");
        });
        it('false to a show object', function(){
            var result = validateStructure(objfortest.fullShowObj);
            assert.equal(result, false, "false to show object");
        });
        it('false to missing payload', function(){
            var result = validateStructure(objfortest.missingPayload);
            assert.equal(result, false, "missing payload property");
        });
        it('false to wrong data type for image', function(){
            var result = validateStructure(objfortest.wrongImageData);
            assert.equal(result, false, "wrong data type for image");
        });
        it('false to wrong data type for slug', function(){
            var result = validateStructure(objfortest.wrongSlugData);
            assert.equal(result, false, "wrong slug type for image");
        });
    });
});