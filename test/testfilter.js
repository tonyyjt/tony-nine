var assert = require('chai').assert;
var rewire = require('rewire');
var ninefilter = rewire('../lib/ninefilter');
var objfortest = require("./objfortest");

var filterWithDrmAndEpisodes = ninefilter.filterWithDrmAndEpisodes;
var makeFilteredShowObject = ninefilter.__get__("makeFilteredShowObject");

describe('ninefilter',function(){
    describe('makeFilteredShowObject', function(){
        var filteredShow = makeFilteredShowObject(objfortest.fullShowObj);
        var numberOfProperties = Object.keys(filteredShow).length;
        it('expect a show object to be filtered down to 3 selected properties', function(){
            assert.equal(numberOfProperties, 3, "3 properties should be made");
        });
        it("has properties slug, title, and image", function(){
            assert.property(filteredShow, "image", "does the new object have an image property?");
            assert.property(filteredShow, "title", "does the new object have a title property?");
            assert.property(filteredShow, "slug", "does the new object have a slug property?");
        });
    });
    describe('filterWithDrmAndEpisodes', function(){
        var responseObj = filterWithDrmAndEpisodes(objfortest.fullObj);
        it('property is named as response', function(){
            assert.property(responseObj, "response", "does the response object have a property called 'response'?");
        });
        it('filtering for DRM and episodes, and outputs with drm is true', function(){
            assert.equal(responseObj.response.length, 1, "only one had drm as true or with episodes");
        });
        var notAcceptMissingImageObj = filterWithDrmAndEpisodes(objfortest.missingImage);
        it("filters out objects with missing image", function(){
			assert.equal(notAcceptMissingImageObj.response.length, 1, "one object had a missing image property, the other didn't");
		});
		var notAcceptMissingSlugObj = filterWithDrmAndEpisodes(objfortest.missingSlug);
        it("filters out objects with missing slug", function(){
			assert.equal(notAcceptMissingSlugObj.response.length, 2, "one object had a missing slug property, 2 other didn't");
		});
		var notAcceptMissingTitleObj = filterWithDrmAndEpisodes(objfortest.missingTitle);
        it("filters out objects with missing title", function(){
			assert.equal(notAcceptMissingTitleObj.response.length, 1, "two object has a missing title property, 1 other didn't");
		});
    });
})
