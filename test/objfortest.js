module.exports = {
    fullObj : {
        payload:
            [{
                country: "UK",
                drm: true,
                episodeCount: 3,
                genre: "Reality",
                image: {
                    "showImage": "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg"
                },
                slug: "show/16kidsandcounting",
                title: "16 Kids and Counting",
            },
            {
                country: "US",
                drm: false,
                episodeCount: 6,
                genre: "Reality",
                image: {
                    showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                slug: "show/16kidsandcounting2",
                title: "16 Kids and Counting2",
            },
            {
                country: "US",
                drm: true,
                episodeCount: 0,
                genre: "Reality",
                image: {
                    showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                slug: "show/16kidsandcounting3",
                title: "16 Kids and Counting3",
            }],
        "skip": 0,
        "take": 10,
        "totalRecords": 75
    },
    fullShowObj : {
        country: "UK",
        drm: true,
        episodeCount: 3,
        genre: "Reality",
        image: {
            "showImage": "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg"
        },
        slug: "show/16kidsandcounting",
        title: "16 Kids and Counting",
    },
    missingPayload:{
        "skip": 0,
        "take": 10,
        "totalRecords": 75
    },
    wrongImageData:{
        payload:
            [{
                country: "UK",
                drm: true,
                episodeCount: 3,
                genre: "Reality",
                image: {
                    "showImage": 1
                },
                slug: "show/16kidsandcounting",
                title: "16 Kids and Counting",
            },
            {
                country: "US",
                drm: false,
                episodeCount: 6,
                genre: "Reality",
                image: {
                    showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                slug: "show/16kidsandcounting2",
                title: "16 Kids and Counting2",
            },
            {
                country: "US",
                drm: true,
                episodeCount: 0,
                genre: "Reality",
                image: {
                    showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                slug: "show/16kidsandcounting3",
                title: "16 Kids and Counting3",
            }]
    },
    wrongSlugData:{
        payload:
            [{
                country: "UK",
                drm: true,
                episodeCount: 3,
                genre: "Reality",
                image: {
                    "showImage": "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                slug: "show/16kidsandcounting",
                title: "16 Kids and Counting",
            },
            {
                country: "US",
                drm: false,
                episodeCount: 6,
                genre: "Reality",
                image: {
                    showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                slug: "show/16kidsandcounting2",
                title: "16 Kids and Counting2",
            },
            {
                country: "US",
                drm: true,
                episodeCount: 0,
                genre: "Reality",
                image: {
                    showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                slug: 123,
                title: "16 Kids and Counting3",
            }]
    },
	missingImage:{
        payload:
            [{
                country: "UK",
                drm: true,
                episodeCount: 3,
                genre: "Reality",
                slug: "show/16kidsandcounting",
                title: "16 Kids and Counting",
            },
            {
                country: "US",
                drm: true,
                episodeCount: 1,
                genre: "Reality",
                image: {
                    showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                slug: "show/16kidsandcounting3",
                title: "16 Kids and Counting3",
            }]
    },
    missingSlug:{
        payload:
            [{
                country: "UK",
                drm: true,
                episodeCount: 3,
                image: {
					"showImage": "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg"
				},
                genre: "Reality",
                slug: "show/16kidsandcounting",
                title: "16 Kids and Counting",
            },
            {
                country: "US",
                drm: true,
                episodeCount: 1,
                genre: "Reality",
                image: {
                    showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                title: "16 Kids and Counting3",
            },
            {
                country: "US",
                drm: true,
                episodeCount: 1,
                genre: "Reality",
                image: {
                    showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                slug: "show/16kidsandcounting3",
                title: "16 Kids and Counting3",
            }]
    },
    missingTitle:{
        payload:
            [{
                country: "UK",
                drm: true,
                episodeCount: 3,
                image: {
					"showImage": "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg"
				},
                genre: "Reality",
                slug: "show/16kidsandcounting",
            },
            {
                country: "US",
                drm: true,
                episodeCount: 1,
                genre: "Reality",
                image: {
                    showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                slug: "show/16kidsandcounting3",
            },
            {
                country: "US",
                drm: true,
                episodeCount: 1,
                genre: "Reality",
                image: {
                    showImage: "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1288.jpg"
                },
                slug: "show/16kidsandcounting3",
                title: "16 Kids and Counting3",
            }]
    }
}
