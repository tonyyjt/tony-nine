var assert = require('chai').assert;
var rewire = require('rewire');
var ninemain = rewire('../lib/ninemain');

var passingjson = '{"somekey": "somevalue"}';
var incompletejson = '{"key": }';

describe('ninemain', function(){
    describe('tryParseJSON', function() {
        var tryParseJSON = ninemain.__get__('tryParseJSON');
        it('parses correct JSON', function(){
            var result = tryParseJSON(passingjson);
            assert.property(result, 'somekey', 'returning the correct property');
        });
        it('false to incomplete JSON', function(){
            var result = tryParseJSON(incompletejson);
            assert.equal(result, false);
        });
        it('false to "true"', function(){
            var result = tryParseJSON('true');
            assert.equal(result, false, 'false to returning true');
        });
        it('false to blank', function(){
            var result = tryParseJSON('');
            assert.equal(result, false, 'false for blank');
        });
    });
});
