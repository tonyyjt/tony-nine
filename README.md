# Welcome to tony-nine service
This is for the nine challenge where I have to filter a JSON POST request, and return a filtered down JSON as per specifications.

####How to use the source code
1. Clone and checkout to local directory
2. Run ```node startserver.js``` in tony-nine directory via terminal
3. The server should be up and running on port 8888
4. Make a JSON POST request to test it (refer to functional testing below)

####How to use the web service
1. The minimum JSON POST request requires a payload property which holds an array of JSON objects with properties:
	* drm of type boolean
	* episodeCount of type integer
	* if these properties exist:
		* show with showImage of type string
		* slug of type string
		* title of type string
2. The expected JSON response is if the above properties exist, a JSON representation of the objects will be filtered down to show, slug and title properties and returned.

3. Example request: [ExampleJSONRequest.json](https://bitbucket.org/tonyyjt/tony-nine/src/2b1b17f2e770710270ee4088c88621157f4832e6/exampleJSONRequest.json?fileviewer=file-view-default)

4. Example response: [ExampleJSONResponse.json](https://bitbucket.org/tonyyjt/tony-nine/src/2b1b17f2e770710270ee4088c88621157f4832e6/exampleJSONResponse.json?fileviewer=file-view-default)

####How to test
#####I've used Mocha and Chai for unit testing. 
1. Install Mocha via npm globally: ```npm install -g mocha```
2. Run ```mocha``` in tony-nine file in terminal
3. You will see the passes and fails of several unit tests
3. You may edit and add tests in the test file

#####Postman for functional testing.
1. Install Postman
2. Select POST method
3. Enter the url of the service
4. In Body, enter the JSON POST request
5. Click send
6. You should see the response at the bottom

Functional tests include:

* Valid JSON, valid schema structure (PASS)
* Valid JSON, invalid schema structure (FAIL)
* Invalid JSON (FAIL)
* Empty
* Missing property
	* image
	* slug
	* title
	* showImage
	* drm
	* episodeCount
* Wrong data type/empty value for the above properties
* boolean
* missing payload property
* empty payload (this currently returns an empty response)