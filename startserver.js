var http = require('http');
var ninemain = require('./lib/ninemain');

var server = http.createServer(ninemain);
var port = Number(process.env.PORT || 8888);
server.listen(port);